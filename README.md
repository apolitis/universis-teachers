### sis-ng-teacher

SIS client application for teachers

## Prerequisites

Node.js version >6.14.3 is required. Visit [Node.js](https://nodejs.org/en/) 
and follow the installation instructions provided for your operating system.

## Installation

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.
Navigate to application directory and execute:

    npm i

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:7002/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Modules

#### MostModule (@themost/angular)

sis-ng-teacher uses MOST Client Module for Angular 2.x-6.x in order to communicate with SIS API Server.

    ...
    this.context.model("students/me/registrations").asQueryable()
          .orderBy('registrationYear desc')
          .thenBy('registrationPeriod desc')
          .getItems().then((res)=> {
              this.registrations = res.value;
    });
    ...

[@themost/client documentation](https://github.com/themost-framework/themost-client/tree/master/modules/%40themost/angular)      


#### SharedModule (app/shared)

SharedModule contains a set of services which can be used by the other application modules.

##### ConfigurationService

Application configuration is loaded on application start-up. Configuration data are static JSON files in src/app/assets/config directory.

    {
        "settings": {
            "app": {
    
            },
            "remote": {
                "server":"http://api.sisnext.io/api/"
            },
            "auth": {
                "authorizeURL":"http://auth.sisnext.io/authorize",
                "logoutURL":"http://auth.sisnext.io/logout?continue=https://localhost:7002/#/auth/login",
                "userProfileURL":"http://auth.sisnext.io/me",
                "oauth2": {
                    "tokenURL": "http://auth.sisnext.io/tokeninfo",
                    "clientID": "2658375048506108",
                    "callbackURL": "http://localhost:7002/#/auth/callback",
                    "scope": [
                        "students"
                    ]
                }
            },
            "localization": {
                "cultures": [ "en","el" ],
                "default": "el"
            }
        }
    }

Note: app.json is the default configuration file. In development mode the configuration service will try to load app.development configuration file.
In production mode the configuration service will try to load app.production.json.

The configuration service initializes also application localization by loading Angular locales and application i18n files. These files are stored in src/assets/i18n directory.

    i18n
        el.json
        en.json

A i18n localization file is typically a collection of key-value pairs of translation data.
        
    {
        "Home": "Home",
        "Dashboard": "Dashboard",
        "Overview":"Overview",
        "Courses":"Courses",
        "TotalByCourseType":"total by course type",
        "Results":"Results",
        "CoursesResults":"courses results",
        "SuccessedCourses": "Successed",
        "FailedCourses": "Failed",
        "AcademicPeriod": "Academic period",
        "Average": "Average",
        "AveragePerPeriod":"Average per period",
        "AveragePerPeriodInfo": "average grade per period",
        "Logout": "Logout",
        "Exams": "Exams",
        "Activity": "Activity",
        "RegistrationsTitle": "Registrations"
    }

It may also contain one or more groups of key-value pairs:

    {
        "Registrations": {
            "Title": "Εγγραφές",
            "AcademicYear":"Ακαδημαϊκό έτος",
            "Period": "Περίοδος",
            "Status": "Κατάσταση",
            "Date": "Ημερομηνία",
            "DateModified": "Τροποποιήθηκε",
            "Details": "Λεπτομέρειες"
        }
    }

Each one of application module may also contain a set of translation data. These files are stored in module directory (e.g. /src/app/registration/i18n)

    src
        + app
            + registrations
                + i18n
                    registrations.el.json
                    registrations.en.json

These localization data are loaded during module start-up.

    //# src/app/registrations/registrations.module.ts
    
    export class RegistrationsModule {
        constructor(private _translateService: TranslateService) {
            environment.languages.forEach((culture) => {
                import(`./i18n/registrations.${culture}.json`).then((translations) => {
                    this._translateService.setTranslation(culture, translations, true);
                });
            });
        }
    }

#### AuthModule (app/auth)

AuthModule contains a set of components and services for user authentication and authorization.

##### AuthenticationService

sis-ng-teacher uses the OAuth2 Implicit Authorization Flow. The configuration settings of OAuth2 server are stored in application configuration:

    ...
    "auth": {
        "authorizeURL":"http://auth.sisnext.io/authorize",
        "logoutURL":"http://auth.sisnext.io/logout?continue=https://localhost:7002/#/auth/login",
        "userProfileURL":"http://auth.sisnext.io/me",
        "oauth2": {
            "tokenURL": "http://auth.sisnext.io/tokeninfo",
            "clientID": "2658375048506108",
            "callbackURL": "http://localhost:7002/#/auth/callback",
            "scope": [
                "students"
            ]
        }
    }
    ...

##### AuthGuard

AuthGuard service is a typical Angular authorization guard. Application uses this guard to allow or deny access to specific application routes.

    //# src/app/app.routing.ts

    ...
    {
            path: '',
            component: FullLayoutComponent,
            canActivate: [
                AuthGuard
            ],
            data: {
                title: 'Home'
            },
            children: [
                {
                    path: 'dashboard',
                    loadChildren: './dashboard/dashboard.module#DashboardModule'
                },
                {
                    path: 'registrations',
                    loadChildren: './registrations/registrations.module#RegistrationsModule'
                }
            ]
        }
    ...
    
AuthGuard uses a configuration file to store these settings (src/app/auth/guards/auth.guard.locations.json)

    [
        {
            "privilege":"Location",
            "target": {
                "url":"^/auth/"
            },
            "mask": 1
        },
        {
            "privilege":"Location",
            "target": {
                "url":"^/error"
            },
            "mask": 1
        },
        {
            "privilege":"Location",
            "account": {
                "name":"Students"
            },
            "target": {
                "url":"^/"
            },
            "mask": 1
        }
    ]

#### ErrorModule (app/error)

ErrorModule contains a set of components and services for error handling.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
