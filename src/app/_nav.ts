export const navigation = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    title: true,
    name: 'Main Menu'
  },
  {
    name: 'Classes.Classes',
    url: '/classes',
    icon: 'icon-puzzle',
    children: [
      {
        name: 'Classes.CurrentClasses',
        url: '/classes/current',
        icon: 'icon-puzzle'
      },
      {
        name: 'Classes.AllClasses',
        url: '/classes/all',
        icon: 'icon-puzzle'
      }
    ]
  },
  {
    name: 'Courses',
    url: '/courses',
    icon: 'icon-book-open'
  },
  {
    name: 'Exams',
    url: '/exams',
    icon: 'icon-note',

  },
  {
    divider: true
  },
  {
    title: true,
    name: 'Μενου Χρήστη',
  },
  {
    name: 'Προφίλ',
    url: '/profile',
    icon: 'icon-user',

  },
  {
    name: 'Γλώσσα',
    url: '/lang',
    icon: 'icon-globe',
    children: [
      {
        name: 'Ελληνικά',
        url: '/lang/el',
        icon: 'icon-cursor'
      },
      {
        name: 'English',
        url: '/lang/en',
        icon: 'icon-cursor'
      }
    ]

  },
  {
    name: 'Αποσύνδεση',
    url: '/auth/logout',
    icon: 'icon-lock',
    variant: 'notice'
  }
];
