import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {HttpClientModule} from "@angular/common/http";
import { AuthenticationService } from './services/authentication.service';
import { UserService } from './services/user.service';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { AuthGuard } from './guards/auth.guard';
import { AuthRoutingModule } from './auth-routing.routing';
import { AuthCallbackComponent } from "./auth-callback.component";
import { TranslateModule } from "@ngx-translate/core";
import { MostModule } from "@themost/angular/module";

@NgModule({
  imports: [
    HttpClientModule,
    CommonModule,
    FormsModule,
    TranslateModule,
    MostModule,
    AuthRoutingModule
  ],
  providers: [
    AuthGuard,
    AuthenticationService,
    UserService
    ],
  declarations: [
    LoginComponent,
    LogoutComponent,
    AuthCallbackComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AuthModule { }
