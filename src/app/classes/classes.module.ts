import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';

import { ClassesRoutingModule } from './classes-routing.module';
import { ClassesHomeComponent } from './components/classes-home/classes-home.component';
import { ClassesAllComponent } from './components/classes-all/classes-all.component';
import { ClassesCurrentComponent } from './components/classes-current/classes-current.component';

@NgModule({
  imports: [
    CommonModule,
    ClassesRoutingModule,
    TranslateModule
  ],
  declarations: [ClassesHomeComponent, ClassesAllComponent, ClassesCurrentComponent]
})
export class ClassesModule {
  constructor(private _translateService: TranslateService) {
    environment.languages.forEach((culture) => {
      import(`./i18n/classes.${culture}.json`).then((translations) => {
        this._translateService.setTranslation(culture, translations, true);
      });
    });
  }
}
