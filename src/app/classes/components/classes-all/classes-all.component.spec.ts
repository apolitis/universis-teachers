import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassesAllComponent } from './classes-all.component';

describe('ClassesAllComponent', () => {
  let component: ClassesAllComponent;
  let fixture: ComponentFixture<ClassesAllComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassesAllComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassesAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
