import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';

import { CoursesRoutingModule } from './courses-routing.module';
import { CoursesHomeComponent } from './components/courses-home/courses-home.component';

@NgModule({
  imports: [
    CommonModule,
    CoursesRoutingModule,
    TranslateModule
  ],
  declarations: [CoursesHomeComponent]
})
export class CoursesModule {
  constructor(private _translateService: TranslateService) {
    environment.languages.forEach((culture) => {
      import(`./i18n/courses.${culture}.json`).then((translations) => {
        this._translateService.setTranslation(culture, translations, true);
      });
    });
  }
}
