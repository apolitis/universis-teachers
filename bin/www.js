/*eslint-env node*/
/*eslint no-var: "off"*/

const path = require('path');
const express = require('express');
const isDevMode = process.env.NODE_ENV === 'development';
const srcDir = '/../dist/server/';

/**
 * Module dependencies.
 */

const app = require(path.join(__dirname + srcDir + 'app'));

const debug = require('debug')('express:app');
const http = require('http');

/**
 * Get port from environment and store in Express.
 */

const port = normalizePort(process.env.PORT || '3101');
app.set('port', port);

//serving static files (in production)
if (!isDevMode) {
    app.use('/',express.static(path.join(__dirname, './../dist/client')));
}

/**
 * Create HTTP server.
 */

const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port, function() { // Eldar added this callback to start browserSync in gulpfile
    if (process.send) {
        process.send('online');
    } else {
        //console.log('The server is running at http://localhost:' + server.get('port'));
    }
});
server.on('error', onError);
server.on('listening', onListening);



/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    const port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    const bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    const addr = server.address();
    const bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    debug('Listening on ' + bind);
}